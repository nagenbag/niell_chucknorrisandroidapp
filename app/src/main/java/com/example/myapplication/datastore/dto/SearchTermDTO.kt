package com.example.myapplication.datastore.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "search_terms_table")
data class SearchTermDTO (
    @PrimaryKey
    @ColumnInfo(name="term")
    val searchTerm: String,

    @ColumnInfo(name="searchResults")
    val searchResults: Int,

    @ColumnInfo(name="date")
    val date: Long
)