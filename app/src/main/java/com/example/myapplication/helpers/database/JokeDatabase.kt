package com.example.myapplication.helpers.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.datastore.dto.SearchTermDTO

@Database(entities = [JokeDTO::class, SearchTermDTO::class], version = 2, exportSchema = false)
abstract class JokeDatabase : RoomDatabase() {
    abstract val jokeDatabaseDAO: JokeDatabaseDAO

    companion object {
        @Volatile
        private var INSTANCE: JokeDatabase? = null
        const val DATA_BASE_NAME = "jokes_saved_database"

        fun getInstance(context: Context): JokeDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        JokeDatabase::class.java,
                        DATA_BASE_NAME
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}
