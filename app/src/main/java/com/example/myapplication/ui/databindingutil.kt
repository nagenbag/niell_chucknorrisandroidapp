package com.example.myapplication.ui

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.helpers.adaptors.RecyclerViewAdaptor
import com.example.myapplication.ui.viewholders.BaseCardModel
import com.example.myapplication.ui.viewholders.RandomJokeModel
import com.example.myapplication.ui.viewholders.SavedJokeModel
import com.squareup.picasso.Picasso


@BindingAdapter("imageUrl", "placeholder")
fun setImageFromUrl(imageView: ImageView, url: String, error: Drawable) {
    Picasso.with(imageView.context)
        .load(url)
        .placeholder(error)
        .into(imageView)
}

@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<BaseCardModel>?) {
    val adapter = recyclerView.adapter as RecyclerViewAdaptor
    adapter.submitList(data)
}

@BindingAdapter("isLoading")
fun bindIsLoading(progressBar: ProgressBar, isLoading: Boolean) {
    if (isLoading) {
        progressBar.visibility = View.VISIBLE
    } else {
        progressBar.visibility = View.GONE
    }
}