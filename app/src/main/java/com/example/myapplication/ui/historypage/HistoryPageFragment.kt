package com.example.myapplication.ui.historypage

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentHistoryPageBinding
import com.example.myapplication.helpers.GenericViewModelFactory
import com.example.myapplication.helpers.adaptors.RecyclerViewAdaptor
import com.example.myapplication.ui.viewholders.SavedJokeModel

class HistoryPageFragment : Fragment() {
    private lateinit var binding: FragmentHistoryPageBinding
    private val viewModel: HistoryPageViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "view model accessed before onViewCreated() fired"
        }

        ViewModelProvider(
            this,
            GenericViewModelFactory(activity.application)
        ).get(HistoryPageViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_history_page, container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adaptor = RecyclerViewAdaptor()
        binding.recyclerView.adapter = adaptor

        // Because the mapping from jokeDTP to SavedJokeModel contains a on click call back, this
        // should not be handled via a binding adaptor
        viewModel.jokes.observe(
            viewLifecycleOwner,
            Observer {
                val newDataSet = it.map { jokeDTO ->
                    SavedJokeModel(
                        jokeDTO
                    ) { viewModel.deleteJoke(jokeDTO) }
                }
                adaptor.submitList(newDataSet)
            }
        )
    }
}