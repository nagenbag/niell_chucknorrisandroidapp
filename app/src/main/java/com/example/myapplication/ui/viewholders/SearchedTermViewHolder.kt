package com.example.myapplication.ui.viewholders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myapplication.R
import com.example.myapplication.databinding.ViewHolderSearchTermBinding
import com.example.myapplication.databinding.ViewHolderSearchedBinding
import com.example.myapplication.datastore.dto.SearchTermDTO

data class SearchedTermModel(
    val data: SearchTermDTO,
    val onAction: (term:String) -> Unit
): BaseCardModel(
    CardType.SEARCHED_TERM
)

class SearchedTermViewHolder(private val itemViewBinding: ViewHolderSearchTermBinding) : BaseViewHolder(itemViewBinding.root)  {
    companion object {
        fun getInstance(parent: ViewGroup): SearchedTermViewHolder {
            val inflater = LayoutInflater.from(parent.context)

            return SearchedTermViewHolder(
                ViewHolderSearchTermBinding.inflate(inflater, parent, false)
            )
        }
    }

    private lateinit var model: SearchedTermModel

    override fun bind(item: BaseCardModel) {
        model = item as SearchedTermModel

        itemViewBinding.searchTermModel = model.data
        itemViewBinding.root.setOnClickListener {
            model.onAction.invoke(model.data.searchTerm)
        }
    }
}