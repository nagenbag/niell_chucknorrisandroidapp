package com.example.myapplication.helpers

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.widget.Toast
import com.example.myapplication.R

fun copyToClipboard(context: Context?, content: String) {
    context?.let {
        val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
        val dataClip = ClipData.newPlainText("Payload", content)
        clipboard?.setPrimaryClip(dataClip)
        Toast.makeText(context, context.getString(R.string.joke_copied), Toast.LENGTH_SHORT)
    }
}