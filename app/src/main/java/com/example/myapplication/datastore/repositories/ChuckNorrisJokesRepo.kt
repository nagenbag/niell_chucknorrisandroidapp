package com.example.myapplication.datastore.repositories

import android.content.Context
import androidx.lifecycle.viewModelScope
import androidx.room.ColumnInfo
import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.datastore.dto.SearchResponseDTO
import com.example.myapplication.datastore.dto.SearchTermDTO
import com.example.myapplication.helpers.database.JokeDatabaseDAO
import com.example.myapplication.helpers.network.RetrofitClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ChuckNorrisJokesRepo(
    private val context: Context,
    private val database: JokeDatabaseDAO
) {
    private val networkClient = RetrofitClient(context).apiClient
    val dataBaseJokes = database.getAllStoredJokes()
    val dataBaseSearchTerms = database.getSearchedTerms()

    suspend fun getRandomJoke() : JokeDTO {
        return withContext(Dispatchers.IO) {
            return@withContext networkClient.getRandomJoke()
        }
    }

    suspend fun searchJokesByQuery(query: String) : SearchResponseDTO {
        return withContext(Dispatchers.IO) {
            return@withContext networkClient.getLisOfJokesWithSearchQuery(query)
        }
    }

    suspend fun addJokeToDB(joke: JokeDTO) {
        return withContext(Dispatchers.IO) {
            database.insertJoke(
                joke
            )
        }
    }

    suspend fun deleteJokeFromDB(joke: JokeDTO) {
        withContext(Dispatchers.IO) {
            database.deleteJoke(joke)
        }
    }

    suspend fun addSearchTermToDB(searchTermDTO: SearchTermDTO) {
        withContext(Dispatchers.IO) {
            database.insertSearchTerm(searchTermDTO)
        }
    }
}
