package com.example.myapplication.ui.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import java.util.*

abstract class BaseCardModel(
    val type: CardType,
    val id: String = UUID.randomUUID().toString()
) {
    enum class CardType {
        UNSPECIFIED,

        RANDOM_JOKE,
        SEARCHED_JOKE,
        SAVED_JOKE,
        SEARCHED_TERM
    }
}

fun getCardTypeFromOrdinal(ordinal: Int): BaseCardModel.CardType {
    return when (ordinal) {
        BaseCardModel.CardType.RANDOM_JOKE.ordinal -> BaseCardModel.CardType.RANDOM_JOKE
        BaseCardModel.CardType.SEARCHED_JOKE.ordinal -> BaseCardModel.CardType.SEARCHED_JOKE
        BaseCardModel.CardType.SAVED_JOKE.ordinal -> BaseCardModel.CardType.SAVED_JOKE
        BaseCardModel.CardType.SEARCHED_TERM.ordinal -> BaseCardModel.CardType.SEARCHED_TERM
        else -> BaseCardModel.CardType.UNSPECIFIED
    }
}

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(item: BaseCardModel)
}