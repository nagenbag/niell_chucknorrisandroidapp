package com.example.myapplication.datastore.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "jokes_table")
data class JokeDTO(
    @SerializedName("id")
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),

    @SerializedName("url")
    @ColumnInfo(name="url")
    val url: String?,

    @SerializedName("icon_url")
    @ColumnInfo(name="iconUrl")
    val iconUrl: String?,

    @SerializedName("value")
    @ColumnInfo(name="value")
    val joke: String?,

    @SerializedName("created_at")
    @ColumnInfo(name="dateCreatedAt")
    val dateCreatedAt: String?,

    @SerializedName("updated_at")
    @ColumnInfo(name="dateLastUpdated")
    val dateLastUpdated: String?
)