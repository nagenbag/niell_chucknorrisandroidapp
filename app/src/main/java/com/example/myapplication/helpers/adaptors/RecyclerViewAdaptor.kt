package com.example.myapplication.helpers.adaptors

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.myapplication.ui.viewholders.*


/**
 * Sets a binding between data and views.
 */
class RecyclerViewAdaptor : ListAdapter<BaseCardModel, BaseViewHolder>(RecyclerViewDiffCallback()) {
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).type.ordinal
    }

    /**
     * This function is used to return the correct view holder based on the CardType enum.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return getBaseCardViewHolder(
            getCardTypeFromOrdinal(viewType),
            parent
        )
    }

    private fun getBaseCardViewHolder(
        viewType: BaseCardModel.CardType,
        parent: ViewGroup
    ): BaseViewHolder {
        return when (viewType){
            BaseCardModel.CardType.UNSPECIFIED -> UnspecifiedViewHolder.getInstance(parent)
            BaseCardModel.CardType.RANDOM_JOKE -> RandomJokeViewHolder.getInstance(parent)
            BaseCardModel.CardType.SEARCHED_JOKE -> SearchedJokeViewHolder.getInstance(parent)
            BaseCardModel.CardType.SAVED_JOKE -> SavedJokeViewHolder.getInstance(parent)
            BaseCardModel.CardType.SEARCHED_TERM -> SearchedTermViewHolder.getInstance(parent)
        }
    }
}

class RecyclerViewDiffCallback : DiffUtil.ItemCallback<BaseCardModel>() {
    override fun areItemsTheSame(oldItem: BaseCardModel, newItem: BaseCardModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: BaseCardModel, newItem: BaseCardModel): Boolean {
        return oldItem == newItem
    }
}




