package com.example.myapplication.datastore.dto

import com.google.gson.annotations.SerializedName

data class SearchResponseDTO(
    @SerializedName("total")
    val resultCount: Int,

    @SerializedName("result")
    val results: List<JokeDTO>
)