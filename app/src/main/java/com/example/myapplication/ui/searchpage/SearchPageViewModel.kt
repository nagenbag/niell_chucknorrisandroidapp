package com.example.myapplication.ui.searchpage

import android.app.Application
import androidx.lifecycle.*
import com.example.myapplication.R
import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.datastore.dto.SearchTermDTO
import com.example.myapplication.datastore.repositories.ChuckNorrisJokesRepo
import com.example.myapplication.helpers.LiveDataEventWrapper
import com.example.myapplication.ui.viewholders.SearchedJokeModel
import kotlinx.coroutines.launch

class SearchPageViewModel(
    private val repo: ChuckNorrisJokesRepo
) : ViewModel() {

    val previousSearchTerms = repo.dataBaseSearchTerms

    private val _data = MutableLiveData<List<SearchedJokeModel>>()
    val data: LiveData<List<SearchedJokeModel>>
        get() = _data

    private val _totalResults = MutableLiveData<String>()
    val totalResults: LiveData<String>
        get() = _totalResults

    private val _showToast = MutableLiveData<Int>()
    val showToast: LiveData<Int>
        get() = _showToast

    private val _showModal = MutableLiveData<LiveDataEventWrapper<Boolean>>()
    val showModal: LiveData<LiveDataEventWrapper<Boolean>>
        get() = _showModal

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading


    fun showLocationDeniedError() {
        _showToast.value = R.string.microphone_denied_permision
    }

    fun searchJokes(searchTerm: String) {
        _isLoading.value = true
        viewModelScope.launch {
            runCatching {
                repo.searchJokesByQuery(searchTerm)
            }.onSuccess {
                _totalResults.value = "Total Results for $searchTerm: ${it.resultCount}"
                _isLoading.value = false
                addSearchTermToDB(
                    SearchTermDTO(
                        searchTerm,
                        it.resultCount,
                        System.currentTimeMillis()
                    )
                )
                val newDataList = it.results.map { jokeDTO ->
                    SearchedJokeModel(
                        jokeDTO
                    ) { jokeToSave ->
                        _showModal.value = LiveDataEventWrapper(true)
                        addJokeToDB(jokeToSave)
                    }
                }
                _data.value = newDataList
            }.onFailure {
                _isLoading.value = false
                _showToast.value = R.string.network_failed
            }
        }
    }

    private fun addSearchTermToDB(searchTermItem: SearchTermDTO) {
        viewModelScope.launch {
            repo.addSearchTermToDB(searchTermItem)
        }
    }

    private fun addJokeToDB(joke: JokeDTO) {
        viewModelScope.launch {
            repo.addJokeToDB(joke)
        }
    }
}