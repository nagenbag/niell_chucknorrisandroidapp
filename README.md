# Niell_ChuckNorrisAndroidApp

_This is a basic application to interface with the https://api.chucknorris.io api. 
The applicion consits of the following three screens:_



**Home screen:**

This home screen will display a random joke to the user on a horizontal recycler view, using a snap to page behaviour. 

Future work on this screen: 

- The api allows the random jokes to be filtered by category. This screen can be updated to allow the user to select a catergory. All new jokes it retrieve should then be of the new catergory. 
- Currently there is no error handling on this screen, so if the end point fail no content will display on the screen
- Currently there is no indication of loading while waiting for a response from the api, this can be remedied using skeleton layouts while waiting for a response.


**Search screen:**

This screen allows the user to search for a specfic joke.

Future work on this screen: 

- Currently there is no error handling on this screen, so if the end point fail no content will display on the screen
- Currently there is no indication of loading while waiting for a response from the api, this can be remedied using skeleton layouts while waiting for a response.

**Saved: screen:**

On the other two screens a user can save a specfic joke. This screen will display all of the users saved jokes. 

Future work on this screen: 

- Add functionality to clear all saved jokes
- Add functionality to filter jokes on various filters, like when the joke was last updated
- Currently the screen will not inform the user if there is no saved jokes, this should be handled.

**General Future work:**
- Currently the dates retrieved from the api is not formated, this should be updated
