package com.example.myapplication.ui.landingpage

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.example.myapplication.databinding.FragmentLandingPageBinding
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.PagerSnapHelper
import com.example.myapplication.R
import com.example.myapplication.helpers.GenericViewModelFactory
import com.example.myapplication.helpers.adaptors.RecyclerViewAdaptor
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class LandingPageFragment : Fragment() {
    private lateinit var binding: FragmentLandingPageBinding

    private val viewModel: LandingPageViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "view model accessed before onViewCreated() fired"
        }

        ViewModelProvider(
            this,
            GenericViewModelFactory(activity.application)
        ).get(LandingPageViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_landing_page, container, false
        )
        binding.lifecycleOwner = this

        binding.mainIllusUrl = getString(R.string.DASH_MAIN_ILLUS_URL)
        binding.landingPageViewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adaptor = RecyclerViewAdaptor()
        binding.recyclerView.adapter = adaptor
        val snapper = PagerSnapHelper()
        snapper.attachToRecyclerView(binding.recyclerView)

        viewModel.showModal.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    MaterialAlertDialogBuilder(requireContext())
                        .setMessage(getString(R.string.view_saved_model))
                        .setNegativeButton(getString(R.string.view_saved)) { _, _ ->
                            findNavController().navigate(
                                LandingPageFragmentDirections.actionNavLandingPageToNavHistoryPage()
                            )
                        }
                        .setPositiveButton(getString(R.string.confirm)) { _, _ ->
                            // Do nothing
                        }
                        .show()
                }
            }
        )

        viewModel.networkCallFailed.observe(
            viewLifecycleOwner,
            Observer {
                Toast.makeText(
                    requireContext(), getString(R.string.network_failed), Toast.LENGTH_LONG
                ).show()
            }
        )

        viewModel.addJokeToList()

        var lastScrollPosition = 0
        binding.recyclerView.setOnScrollChangeListener { _, _, _, _, _ ->
            val layoutManager = binding.recyclerView.layoutManager
            val snapView = snapper.findSnapView(layoutManager)
            snapView?.let {
                val snapPosition = layoutManager?.getPosition(snapView)
                if (lastScrollPosition != snapPosition && snapPosition != null) {
                    lastScrollPosition = snapPosition
                    if (adaptor.itemCount - lastScrollPosition < 3) {
                        viewModel.addJokeToList()
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }
}