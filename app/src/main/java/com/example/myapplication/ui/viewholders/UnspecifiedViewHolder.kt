package com.example.myapplication.ui.viewholders

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myapplication.R
import kotlinx.android.synthetic.main.vh_unspecified.view.*

data class TodoModel(
    val data: String
): BaseCardModel(
    CardType.UNSPECIFIED
)

class UnspecifiedViewHolder(itemView: View) : BaseViewHolder(itemView)  {
    companion object {
        fun getInstance(parent: ViewGroup): UnspecifiedViewHolder {
            val inflater = LayoutInflater.from(parent.context)

            return UnspecifiedViewHolder(
                inflater.inflate(
                    R.layout.vh_unspecified,
                    parent,
                    false
                )
            )
        }
    }

    private lateinit var model: BaseCardModel

    override fun bind(item: BaseCardModel) {
        model = item

        when (model) {
            is TodoModel -> {
                itemView.text.text = (model as TodoModel).data
            }
            else -> {
                itemView.text.text = "Unrecognized model received: $model"
            }
        }
    }
}
