package com.example.myapplication.ui.historypage

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.datastore.repositories.ChuckNorrisJokesRepo
import kotlinx.coroutines.launch

class HistoryPageViewModel(
    private val repo: ChuckNorrisJokesRepo
) : ViewModel() {

    val jokes = repo.dataBaseJokes

    fun deleteJoke(joke: JokeDTO) {
        viewModelScope.launch {
            repo.deleteJokeFromDB(joke)
        }
    }
}