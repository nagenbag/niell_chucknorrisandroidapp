package com.example.myapplication.ui.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.myapplication.R
import com.example.myapplication.databinding.ViewHolderJokeBinding
import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.helpers.copyToClipboard
import com.squareup.picasso.Picasso

data class RandomJokeModel(
    val joke: JokeDTO,
    val saveJokeAction: (JokeDTO) -> Unit
): BaseCardModel(
    CardType.RANDOM_JOKE
)

class RandomJokeViewHolder(private val itemViewBinding: ViewHolderJokeBinding) : BaseViewHolder(itemViewBinding.root)  {
    companion object {
        fun getInstance(parent: ViewGroup): RandomJokeViewHolder {
            val inflater = LayoutInflater.from(parent.context)

            return RandomJokeViewHolder(
                ViewHolderJokeBinding.inflate(inflater, parent, false)
            )
        }
    }

    private lateinit var model: RandomJokeModel

    override fun bind(item: BaseCardModel) {
        model = item as RandomJokeModel
        itemViewBinding.jokeModel = model.joke

        itemViewBinding.btnCopy.setOnClickListener {
            copyToClipboard(itemViewBinding.root.context, model.joke.joke ?: "")
        }

        itemViewBinding.btnSaveJoke.setOnClickListener {
            model.saveJokeAction(model.joke)
        }
    }
}
