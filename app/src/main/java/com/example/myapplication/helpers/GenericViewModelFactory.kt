package com.example.myapplication.helpers

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.datastore.repositories.ChuckNorrisJokesRepo
import com.example.myapplication.helpers.database.JokeDatabase
import com.example.myapplication.ui.historypage.HistoryPageViewModel
import com.example.myapplication.ui.landingpage.LandingPageViewModel
import com.example.myapplication.ui.searchpage.SearchPageViewModel
import java.lang.IllegalArgumentException

@Suppress("UNCHECKED_CAST")
class GenericViewModelFactory(
    private val application: Application
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        val database = JokeDatabase.getInstance(
            application
        ).jokeDatabaseDAO

        val repo = ChuckNorrisJokesRepo(
            application,
            database
        )

        return when {
            modelClass.isAssignableFrom(LandingPageViewModel::class.java) -> {
                LandingPageViewModel(repo) as T
            }
            modelClass.isAssignableFrom(SearchPageViewModel::class.java) -> {
                SearchPageViewModel(repo) as T
            }
            modelClass.isAssignableFrom(HistoryPageViewModel::class.java) -> {
                HistoryPageViewModel(repo) as T
            }
            else -> throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}