package com.example.myapplication.helpers.network

import android.app.Application
import android.content.Context
import com.example.myapplication.R
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient(private val context: Context) {
    companion object {
        private const val TIMEOUT: Long = 300
//        private const val BASE_URL: String = "https://api.chucknorris.io"
    }

    val apiClient = getClient().create(ChuckNorrisApi::class.java)

    fun getClient(): Retrofit {
        val client = OkHttpClient.Builder()
        client.addInterceptor(
            getLoggingInterceptor()
        )
        client.readTimeout(TIMEOUT, TimeUnit.SECONDS)
        client.writeTimeout(TIMEOUT, TimeUnit.SECONDS)

        val baseUrl = context.getString(R.string.API_ROOT_URL)

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }

    private fun getLoggingInterceptor() : Interceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)
    }
}