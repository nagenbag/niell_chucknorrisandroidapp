package com.example.myapplication.ui.landingpage

import android.util.Log
import androidx.lifecycle.*
import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.datastore.repositories.ChuckNorrisJokesRepo
import com.example.myapplication.helpers.LiveDataEventWrapper
import com.example.myapplication.helpers.database.JokeDatabaseDAO
import com.example.myapplication.ui.viewholders.RandomJokeModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LandingPageViewModel(
    private val repo: ChuckNorrisJokesRepo
) : ViewModel() {
    private val _data = MutableLiveData<List<RandomJokeModel>>()
    val data: LiveData<List<RandomJokeModel>>
        get() = _data

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _showModal = MutableLiveData<LiveDataEventWrapper<Boolean>>()
    val showModal: LiveData<LiveDataEventWrapper<Boolean>>
        get() = _showModal

    private val _networkCallFailed = MutableLiveData<Boolean>()
    val networkCallFailed: LiveData<Boolean>
        get() = _networkCallFailed

    init {
        _isLoading.value = true
        addJokeToList()
        addJokeToList()
        addJokeToList()
    }

    fun addJokeToList() {
        viewModelScope.launch {
            runCatching {
                repo.getRandomJoke()
            }.onSuccess {
                handleResponse(it)
            }
        }
    }

    private fun handleResponse(responseItem: JokeDTO) {
        val oldList = _data.value?.toMutableList() ?: mutableListOf()
        oldList.add(
            RandomJokeModel(
                responseItem
            ) { jokeDTO ->
                _showModal.value = LiveDataEventWrapper(true)
                addJokeToDB(jokeDTO)
            }
        )
        _isLoading.value = false
        _data.value = oldList
    }

    private fun addJokeToDB(joke: JokeDTO) {
        viewModelScope.launch {
            repo.addJokeToDB(joke)
        }
    }
}