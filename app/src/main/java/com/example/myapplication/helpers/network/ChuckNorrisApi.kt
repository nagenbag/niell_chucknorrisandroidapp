package com.example.myapplication.helpers.network

import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.datastore.dto.SearchResponseDTO
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ChuckNorrisApi {
    @GET("/jokes/random")
    suspend fun getRandomJoke() : JokeDTO

    @GET("/jokes/search")
    suspend fun getLisOfJokesWithSearchQuery(
        @Query("query") query: String
    ) : SearchResponseDTO
}