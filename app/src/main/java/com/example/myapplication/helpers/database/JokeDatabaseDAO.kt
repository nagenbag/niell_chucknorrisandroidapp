package com.example.myapplication.helpers.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.datastore.dto.SearchTermDTO

@Dao
interface JokeDatabaseDAO {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertJoke(joke: JokeDTO)

    @Query("DELETE FROM jokes_table")
    suspend fun clear()

    @Delete
    suspend fun deleteJoke(item: JokeDTO)

    @Query("SELECT * FROM jokes_table")
    fun getAllStoredJokes(): LiveData<List<JokeDTO>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSearchTerm(searchTerm: SearchTermDTO)

    @Query("SELECT * FROM search_terms_table ORDER BY date")
    fun getSearchedTerms(): LiveData<List<SearchTermDTO>>
}

