package com.example.myapplication.ui.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.myapplication.R
import com.example.myapplication.databinding.ViewHolderSearchedBinding
import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.helpers.copyToClipboard
import com.squareup.picasso.Picasso

data class SearchedJokeModel(
    val joke: JokeDTO,
    val saveJokeAction: (JokeDTO) -> Unit
): BaseCardModel(
    CardType.SEARCHED_JOKE
)

class SearchedJokeViewHolder(private val itemViewBinding: ViewHolderSearchedBinding) : BaseViewHolder(itemViewBinding.root)  {
    companion object {
        fun getInstance(parent: ViewGroup): SearchedJokeViewHolder {
            val inflater = LayoutInflater.from(parent.context)

            return SearchedJokeViewHolder(
                ViewHolderSearchedBinding.inflate(inflater, parent, false)
            )
        }
    }

    private lateinit var model: SearchedJokeModel

    override fun bind(item: BaseCardModel) {
        model = item as SearchedJokeModel
        itemViewBinding.jokeModel = model.joke

        itemViewBinding.btnCopy.setOnClickListener {
            copyToClipboard(itemViewBinding.root.context, model.joke.joke ?: "")
        }

        itemViewBinding.btnSaveJoke.setOnClickListener {
            model.saveJokeAction(model.joke)
        }
    }
}