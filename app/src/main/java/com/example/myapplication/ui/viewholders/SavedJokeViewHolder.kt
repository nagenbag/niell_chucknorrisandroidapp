package com.example.myapplication.ui.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.example.myapplication.R
import com.example.myapplication.databinding.ViewHolderSavedBinding
import com.example.myapplication.datastore.dto.JokeDTO
import com.example.myapplication.helpers.copyToClipboard
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.squareup.picasso.Picasso

data class SavedJokeModel(
    val joke: JokeDTO,
    val onDeleteAction: (JokeDTO) -> Unit
): BaseCardModel(
    CardType.SAVED_JOKE
)

class SavedJokeViewHolder(private val itemViewBinding: ViewHolderSavedBinding) : BaseViewHolder(itemViewBinding.root)  {
    companion object {
        fun getInstance(parent: ViewGroup): SavedJokeViewHolder {
            val inflater = LayoutInflater.from(parent.context)

            return SavedJokeViewHolder(
                ViewHolderSavedBinding.inflate(inflater, parent, false)
            )
        }
    }

    private lateinit var model: SavedJokeModel

    override fun bind(item: BaseCardModel) {
        model = item as SavedJokeModel
        itemViewBinding.jokeModel = model.joke

        itemViewBinding.btnCopy.setOnClickListener {
            copyToClipboard(itemViewBinding.root.context, model.joke.joke ?: "")
        }

        itemViewBinding.btnDelete.setOnClickListener {
            val context = itemViewBinding.root.context
            MaterialAlertDialogBuilder(context)
                .setMessage(context.getString(R.string.delete_modal))
                .setNegativeButton(context.getString(R.string.cancel)) { _, _ ->
                    // Do nothing
                }
                .setPositiveButton(context.getString(R.string.delete)) { _, _ ->
                    model.onDeleteAction(model.joke)
                }
                .show()
        }
    }
}