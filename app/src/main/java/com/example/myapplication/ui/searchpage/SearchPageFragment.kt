package com.example.myapplication.ui.searchpage

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.activity.result.contract.ActivityResultContracts
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentSearchPageBinding
import com.example.myapplication.helpers.GenericViewModelFactory
import com.example.myapplication.helpers.adaptors.RecyclerViewAdaptor
import com.example.myapplication.ui.viewholders.SearchedTermModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.util.*

class SearchPageFragment : Fragment() {

    private lateinit var binding: FragmentSearchPageBinding
    private lateinit var speechRecognizer: SpeechRecognizer

    private var isRecordingTerm: Boolean = false

    private val speechRecognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
        putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
    }

    private val viewModel: SearchPageViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "view model accessed before onViewCreated() fired"
        }

        ViewModelProvider(
            this,
            GenericViewModelFactory(activity.application)
        ).get(SearchPageViewModel::class.java)
    }

    private val askPermission = registerForActivityResult(ActivityResultContracts.RequestPermission()) { result ->
        if(result){
            setupMicrophoneFeature()
        }else{
            viewModel.showLocationDeniedError()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_search_page, container, false
        )
        binding.lifecycleOwner = this
        binding.searchPageViewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (confirmRecordPermission()) {
            setupMicrophoneFeature()
        }

        binding.searchPageViewModel = viewModel

        val previousSearchesAdaptor = RecyclerViewAdaptor()
        binding.recyclerViewPrevious.adapter = previousSearchesAdaptor

        val searchResultAdaptor = RecyclerViewAdaptor()
        binding.recyclerViewResults.adapter = searchResultAdaptor

        viewModel.previousSearchTerms.observe(
            viewLifecycleOwner,
            Observer {
                val displayData = it.map { searchTerm ->
                    SearchedTermModel(
                        searchTerm
                    ) {
                        viewModel.searchJokes(searchTerm.searchTerm)
                    }
                }
                previousSearchesAdaptor.submitList(displayData)
            }
        )

        viewModel.totalResults.observe(
            viewLifecycleOwner,
            Observer {
                binding.txtResultsCount.visibility = View.VISIBLE
                binding.txtResultsCount.text = it
            }
        )

        viewModel.showToast.observe(
            viewLifecycleOwner,
            Observer {
                Toast.makeText(requireContext(), getText(it), Toast.LENGTH_LONG).show()
            }
        )

        viewModel.showModal.observe(
            viewLifecycleOwner,
            Observer {
                it.getContentIfNotHandled()?.let {
                    MaterialAlertDialogBuilder(requireContext())
                        .setMessage(getString(R.string.delete_modal))
                        .setNegativeButton(getString(R.string.view_saved)) { _, _ ->
                            findNavController().navigate(
                                SearchPageFragmentDirections.actionNavSearchPageToNavHistoryPage()
                            )
                        }
                        .setPositiveButton(getString(R.string.confirm)) { _, _ ->
                            // Do nothing
                        }
                        .show()
                }
            }
        )

        binding.btnSearch.setOnClickListener {
            viewModel.searchJokes(binding.edtSearchJokes.text.toString())
            binding.edtSearchJokes.text = SpannableStringBuilder("")
            binding.edtSearchJokes.clearFocus()

            binding.motionLayout.transitionToStart()

            val manager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.hideSoftInputFromWindow(binding.root.windowToken, 0)
        }

        binding.btnMicrophone.setOnClickListener {
            if (confirmRecordPermission()) {
                if (isRecordingTerm) {
                    isRecordingTerm = false

                    binding.btnMicrophone.background = ContextCompat.getDrawable(
                        requireContext(),
                        R.color.white
                    )
                    speechRecognizer.stopListening()
                } else {
                    isRecordingTerm = true

                    binding.btnMicrophone.background = ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.shape_circle
                    )
                    speechRecognizer.startListening(speechRecognizerIntent)
                }
            } else {
                askPermission.launch(Manifest.permission.RECORD_AUDIO)
            }
        }
    }

    private fun confirmRecordPermission() : Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(), Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun setupMicrophoneFeature() {
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(requireContext())
        speechRecognizer.setRecognitionListener(object : RecognitionListener {
            override fun onReadyForSpeech(bundle: Bundle) {}
            override fun onBeginningOfSpeech() {
                binding.edtSearchJokes.setText("")
                binding.edtSearchJokes.hint = getString(R.string.microphone_listening)
            }

            override fun onRmsChanged(v: Float) {}
            override fun onBufferReceived(bytes: ByteArray) {}
            override fun onEndOfSpeech() {}
            override fun onError(i: Int) {}
            override fun onResults(bundle: Bundle) {
                isRecordingTerm = false
                binding.btnMicrophone.background = ContextCompat.getDrawable(
                    requireContext(),
                    R.color.white
                )
                val data = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                data?.firstOrNull()?.let {
                    binding.edtSearchJokes.setText(it)
                }
            }

            override fun onPartialResults(bundle: Bundle) {}
            override fun onEvent(i: Int, bundle: Bundle) {}
        })
    }
}